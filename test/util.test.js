import {
    straddlesAntimeridian,
    crossingPoints,
    bringLonWithinBounds,
    antimeridianIntersect,
} from '../src/util';

describe('Detecting if points straddle antimeridian', () => {
    test('Does not straddle IDL', () => {
        // Obvious
        expect(straddlesAntimeridian([1, 0], [10, 0])).toEqual(false);
        expect(straddlesAntimeridian([-1, 0], [-10, 0])).toEqual(false);
        expect(straddlesAntimeridian([120, 0], [45, 0])).toEqual(false);
        expect(straddlesAntimeridian([-120, 0], [-45, 0])).toEqual(false);

        // Crossing meridian, not antimeridian
        expect(straddlesAntimeridian([-1, 0], [1, 0])).toEqual(false);
        expect(straddlesAntimeridian([1, 0], [-1, 0])).toEqual(false);

        expect(straddlesAntimeridian([-89, 0], [89, 0])).toEqual(false);
        expect(straddlesAntimeridian([89, 0], [-89, 0])).toEqual(false);

        expect(straddlesAntimeridian([179, 0], [-1, 0])).toEqual(false);
        expect(straddlesAntimeridian([-179, 0], [1, 0])).toEqual(false);

        // Coordinates > 180 or < -180
        expect(straddlesAntimeridian([-359, 0], [-361, 0])).toEqual(false); // lon -1 to 1
        expect(straddlesAntimeridian([-361, 0], [-359, 0])).toEqual(false);

        expect(straddlesAntimeridian([359, 0], [361, 0])).toEqual(false); // lon -1 to 1
        expect(straddlesAntimeridian([361, 0], [359, 0])).toEqual(false);

        // Edge cases
        expect(straddlesAntimeridian([180, 0], [179, 0])).toEqual(false);
        expect(straddlesAntimeridian([-180, 0], [-179, 0])).toEqual(false);

        expect(straddlesAntimeridian([90, 0], [-90, 0])).toEqual(false);
        expect(straddlesAntimeridian([-90, 0], [90, 0])).toEqual(false);

        expect(straddlesAntimeridian([0, 0], [0, 0])).toEqual(false);
        expect(straddlesAntimeridian([1, 0], [1, 0])).toEqual(false);
        expect(straddlesAntimeridian([-1, 0], [-1, 0])).toEqual(false);
    });

    test('Straddles IDL', () => {
        expect(straddlesAntimeridian([-179, 0], [179, 0])).toEqual(true);
        expect(straddlesAntimeridian([179, 0], [-179, 0])).toEqual(true);

        expect(straddlesAntimeridian([91, 0], [-91, 0])).toEqual(true);
        expect(straddlesAntimeridian([-91, 0], [91, 0])).toEqual(true);

        // Coordinates > 180 or < -180
        expect(straddlesAntimeridian([190, 0], [530, 0])).toEqual(true);
        expect(straddlesAntimeridian([-530, 0], [-190, 0])).toEqual(true);

        expect(straddlesAntimeridian([0, 0], [360, 0])).toEqual(true);
    });

    test('Bring Within Bounds', () => {
        expect(bringLonWithinBounds(0)).toEqual(0);
        expect(bringLonWithinBounds(1)).toEqual(1);
        expect(bringLonWithinBounds(-1)).toEqual(-1);
        expect(bringLonWithinBounds(180)).toEqual(180);
        expect(bringLonWithinBounds(-180)).toEqual(-180);

        expect(bringLonWithinBounds(181)).toEqual(-179);
        expect(bringLonWithinBounds(-181)).toEqual(179);

        expect(bringLonWithinBounds(360)).toEqual(0);
        expect(bringLonWithinBounds(-360)).toEqual(0);
    });
});

describe('Detecting where lines cross antimeridian', () => {
    test('Does not cross antimeridian', () => {
        const basic = [
            [1, 0],
            [10, 0],
        ];

        const crossesMeridian = [
            [-10, 0],
            [10, 0],
        ];

        const long = [
            [-86.39, 39.80],
            [-84.55, 33.68],
            [-77.08, 38.89],
            [-73.91, 40.74],
            [-87.40, 41.77],
            [-93.25, 44.90],
        ];

        const singlePoint = [
            [0, 0],
        ];

        const empty = [];

        expect(crossingPoints(basic)).toEqual([]);
        expect(crossingPoints(crossesMeridian)).toEqual([]);
        expect(crossingPoints(long)).toEqual([]);
        expect(crossingPoints(singlePoint)).toEqual([]);
        expect(crossingPoints(empty)).toEqual([]);
    });

    test('Does cross antimeridian', () => {
        const basic = [
            [-170, 0],
            [170, 0],
        ];

        const long = [
            [139.79, 35.71], // Tokyo
            [131.83, 43.13], // Vladivostok
            [158.59, 53.04], // Petropavlovsk-Kamchatsky
            [-165.42, 64.51], // Nome
            [-149.99, 61.18], // Anchorage
            [-123.18, 49.32], // Vancouver
        ];

        const multipleCrossings = [
            [-170, -20],
            [170, -20],
            [170, 20],
            [-170, 20],
            [-170, -20],
        ];

        expect(crossingPoints(basic)).toEqual([0]);
        expect(crossingPoints(long)).toEqual([2]);
        expect(crossingPoints(multipleCrossings)).toEqual([0, 2]);
    });
});

describe('Calculating antimeridian intersection', () => {
    test('Simple Coordinates', () => {
        expect(antimeridianIntersect(
            [-170, 10],
            [170, -10],
        )).toBeCloseTo(0);

        expect(antimeridianIntersect(
            [170, -10],
            [-170, 10],
        )).toBeCloseTo(0);

        expect(antimeridianIntersect(
            [-1, 10],
            [1, 10],
        )).toBeCloseTo(10);

        expect(antimeridianIntersect(
            [172.66, -43.51], // Christchurch
            [-149.59, -17.51], // Papeete
        )).toBeCloseTo(-38.45);
    });
});
