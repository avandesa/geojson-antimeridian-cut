import { splitPolygon, splitMultiPolygon } from '../../src/geometry/polygons';

describe('Polygons', () => {
    describe('Simple Polygons', () => {
        test('Should not cut polygon with no crossings', () => {
            const polygon = {
                type: 'Polygon',
                coordinates: [
                    [
                        [40, 50],
                        [50, 50],
                        [50, 40],
                        [40, 40],
                        [40, 50],
                    ],
                ],
            };

            expect(splitPolygon(polygon)).toEqual(polygon);
        });

        test('Should cut polygon with two points on each side (square)', () => {
            const polygon = {
                type: 'Polygon',
                coordinates: [
                    [
                        [170, -10],
                        [-170, -10],
                        [-170, 10],
                        [170, 10],
                        [170, -10],
                    ],
                ],
            };

            expect(splitPolygon(polygon)).toEqual({
                type: 'MultiPolygon',
                coordinates: [
                    [
                        [
                            [180, 10],
                            [170, 10],
                            [170, -10],
                            [180, -10],
                            [180, 10],
                        ],
                    ],
                    [
                        [
                            [-180, -10],
                            [-170, -10],
                            [-170, 10],
                            [-180, 10],
                            [-180, -10],
                        ],
                    ],
                ],
            });
        });

        test('Should cut polygon with one point across (triangle)', () => {
            const polygon = {
                type: 'Polygon',
                coordinates: [
                    [
                        [160, -10],
                        [-160, 0],
                        [160, 10],
                        [160, -10],
                    ],
                ],
            };

            expect(splitPolygon(polygon)).toEqual({
                type: 'MultiPolygon',
                coordinates: [
                    [
                        [
                            [180, 5],
                            [160, 10],
                            [160, -10],
                            [180, -5],
                            [180, 5],
                        ],
                    ],
                    [
                        [
                            [-180, -5],
                            [-160, 0],
                            [-180, 5],
                            [-180, -5],
                        ],
                    ],
                ],
            });
        });

        test('Should cut polygon where first point is immediately after crossing', () => {
            // Makes the logic a bit weird
            const polygon = {
                type: 'Polygon',
                coordinates: [
                    [
                        [-170, -10],
                        [-170, 10],
                        [170, 10],
                        [170, -10],
                        [-170, -10],
                    ],
                ],
            };

            expect(splitPolygon(polygon)).toEqual({
                type: 'MultiPolygon',
                coordinates: [
                    [
                        [
                            [180, 10],
                            [170, 10],
                            [170, -10],
                            [180, -10],
                            [180, 10],
                        ],
                    ],
                    [
                        [
                            [-180, -10],
                            [-170, -10],
                            [-170, 10],
                            [-180, 10],
                            [-180, -10],
                        ],
                    ],
                ],
            });
        });

        test('Should cut reverse polygon', () => {
            // Though linear rings should follow the right-hand rule, we should be able to properly
            // cut rings that go clockwise so we can cut holes properly
            const polygon = {
                type: 'Polygon',
                coordinates: [
                    [
                        [-170, -10],
                        [170, -10],
                        [170, 10],
                        [-170, 10],
                        [-170, -10],
                    ],
                ],
            };

            expect(splitPolygon(polygon)).toEqual({
                type: 'MultiPolygon',
                coordinates: [
                    [
                        [
                            [-180, 10],
                            [-170, 10],
                            [-170, -10],
                            [-180, -10],
                            [-180, 10],
                        ],
                    ],
                    [
                        [
                            [180, -10],
                            [170, -10],
                            [170, 10],
                            [180, 10],
                            [180, -10],
                        ],
                    ],
                ],
            });
        });

        test('Should cut reverse polygon where first point is immediately after crossing', () => {
            const polygon = {
                type: 'Polygon',
                coordinates: [
                    [
                        [-179, 1],
                        [-179, -1],
                        [179, -1],
                        [179, 1],
                        [-179, 1],
                    ],
                ],
            };

            expect(splitPolygon(polygon)).toEqual({
                type: 'MultiPolygon',
                coordinates: [
                    [
                        [
                            [-180, 1],
                            [-179, 1],
                            [-179, -1],
                            [-180, -1],
                            [-180, 1],
                        ],
                    ],
                    [
                        [
                            [180, -1],
                            [179, -1],
                            [179, 1],
                            [180, 1],
                            [180, -1],
                        ],
                    ],
                ],
            });
        });

        test('Should cut irregular polygon', () => {
            const polygon = {
                type: 'Polygon',
                coordinates: [
                    [
                        [175, 10],
                        [-160, -10],
                        [-160, 10],
                        [175, -10],
                        [175, 10],
                    ],
                ],
            };

            expect(splitPolygon(polygon)).toEqual({
                type: 'MultiPolygon',
                coordinates: [
                    [
                        [
                            [-180, 6],
                            [-160, -10],
                            [-160, 10],
                            [-180, -6],
                            [-180, 6],
                        ],
                    ],
                    [
                        [
                            [180, -6],
                            [175, -10],
                            [175, 10],
                            [180, 6],
                            [180, -6],
                        ],
                    ],
                ],
            });
        });
    });

    describe('Polygons with multiple crossings', () => {
        test('Should cut polygon with 4 crossings', () => {
            const polygon = {
                type: 'Polygon',
                // Counter-clockwise just 'cause
                coordinates: [
                    [
                        [-179, 4],
                        [179, 2],
                        [-179, 0],
                        [178, 0],
                        [178, 4],
                        [-179, 4],
                    ],
                ],
            };

            expect(splitPolygon(polygon)).toEqual({
                type: 'MultiPolygon',
                coordinates: [
                    [
                        [
                            [-180, 4],
                            [-179, 4],
                            [-180, 3],
                            [-180, 4],
                        ],
                    ],
                    [
                        [
                            [-180, 1],
                            [-179, 0],
                            [-180, 0],
                            [-180, 1],
                        ],
                    ],
                    [
                        [
                            [180, 0],
                            [178, 0],
                            [178, 4],
                            [180, 4],
                            [180, 3],
                            [179, 2],
                            [180, 1],
                            [180, 0],
                        ],
                    ],
                ],
            });
        });

        test('Should cut polygon with 6 crossings', () => {
            const polygon = {
                type: 'Polygon',
                coordinates: [
                    [
                        [179, 4],
                        [178, 0],
                        [-179, 0],
                        [179, 2],
                        [-179, 4],
                        [-178, 8],
                        [179, 8],
                        [-179, 6],
                        [179, 4],
                    ],
                ],
            };

            expect(splitPolygon(polygon)).toEqual({
                type: 'MultiPolygon',
                coordinates: [
                    [
                        [
                            [180, 8],
                            [179, 8],
                            [180, 7],
                            [180, 8],
                        ],
                    ],
                    [
                        [
                            [180, 1],
                            [179, 2],
                            [180, 3],
                            [180, 5],
                            [179, 4],
                            [178, 0],
                            [180, 0],
                            [180, 1],
                        ],
                    ],
                    [
                        [
                            [-180, 0],
                            [-179, 0],
                            [-180, 1],
                            [-180, 0],
                        ],
                    ],
                    [
                        [
                            [-180, 3],
                            [-179, 4],
                            [-178, 8],
                            [-180, 8],
                            [-180, 7],
                            [-179, 6],
                            [-180, 5],
                            [-180, 3],
                        ],
                    ],
                ],
            });
        });
    });

    describe('Polygons with holes', () => {
        test('Should not cut polygon with hole', () => {
            const polygon = {
                type: 'Polygon',
                coordinates: [
                    [
                        [0, 0],
                        [3, 0],
                        [3, 3],
                        [0, 3],
                        [0, 0],
                    ],
                    [
                        [1, 1],
                        [1, 2],
                        [2, 2],
                        [2, 1],
                        [1, 1],
                    ],
                ],
            };

            expect(splitPolygon(polygon)).toEqual(polygon);
        });

        test('Should cut polygon but not hole', () => {
            const hole = [
                [170, 10],
                [170, 0],
                [175, 0],
                [175, 10],
                [170, 10],
            ];

            const polygon = {
                type: 'Polygon',
                coordinates: [
                    [
                        [-160, -20],
                        [160, -20],
                        [160, 20],
                        [-160, 20],
                        [-160, -20],
                    ],
                    hole,
                ],
            };

            expect(splitPolygon(polygon)).toEqual({
                type: 'MultiPolygon',
                coordinates: [
                    [
                        [
                            [-180, 20],
                            [-160, 20],
                            [-160, -20],
                            [-180, -20],
                            [-180, 20],
                        ],
                        hole,
                    ],
                    [
                        [
                            [180, -20],
                            [160, -20],
                            [160, 20],
                            [180, 20],
                            [180, -20],
                        ],
                        hole,
                    ],
                ],
            });
        });

        test('Should cut hole but not polygon', () => {
            // Edge case, I'll deal with this later
            const poly = [
                [160, 20],
                [160, 0],
                [175, 0],
                [175, 20],
                [160, 20],
            ];

            const polygon = {
                type: 'Polygon',
                coordinates: [
                    poly,
                    [
                        [-170, -10],
                        [-170, 10],
                        [170, 10],
                        [170, -10],
                        [-170, -10],
                    ],
                ],
            };

            expect(splitPolygon(polygon)).toEqual({
                type: 'Polygon',
                coordinates: [
                    poly,
                    [
                        [180, 10],
                        [170, 10],
                        [170, -10],
                        [180, -10],
                        [180, 10],
                    ],
                    [
                        [-180, -10],
                        [-170, -10],
                        [-170, 10],
                        [-180, 10],
                        [-180, -10],
                    ],
                ],
            });
        });

        test('Should cut polygon and hole', () => {
            const polygon = {
                type: 'Polygon',
                coordinates: [
                    [
                        [-160, -20],
                        [160, -20],
                        [160, 20],
                        [-160, 20],
                        [-160, -20],
                    ],
                    [
                        [170, 10],
                        [170, -10],
                        [-170, -10],
                        [-170, 10],
                        [170, 10],
                    ],
                ],
            };

            const cutHole = [
                [
                    [180, 10],
                    [170, 10],
                    [170, -10],
                    [180, -10],
                    [180, 10],
                ],
                [
                    [-180, -10],
                    [-170, -10],
                    [-170, 10],
                    [-180, 10],
                    [-180, -10],
                ],
            ];

            expect(splitPolygon(polygon)).toEqual({
                type: 'MultiPolygon',
                coordinates: [
                    [
                        [
                            [-180, 20],
                            [-160, 20],
                            [-160, -20],
                            [-180, -20],
                            [-180, 20],
                        ],
                        ...cutHole,
                    ],
                    [
                        [
                            [180, -20],
                            [160, -20],
                            [160, 20],
                            [180, 20],
                            [180, -20],
                        ],
                        ...cutHole,
                    ],
                ],
            });
        });
    });
});

describe('MultiPolygons', () => {
    test('Should not cut MultiPolygons', () => {
        const multiPolygon = {
            type: 'MultiPolygon',
            coordinates: [
                [
                    [
                        [10, 10],
                        [20, 10],
                        [20, 20],
                        [10, 10],
                        [10, 10],
                    ],
                ],
                [
                    [
                        [-2, -2],
                        [2, -2],
                        [2, 2],
                        [-2, 2],
                        [-2, -2],
                    ],
                    [
                        [-1, -1],
                        [-1, 1],
                        [1, 0],
                        [-1, -1],
                    ],
                ],
            ],
        };

        expect(splitMultiPolygon(multiPolygon)).toEqual(multiPolygon);
    });

    test('Should cut one of two in MultiPolygons', () => {
        const oneOfTwo = {
            type: 'MultiPolygon',
            coordinates: [
                [
                    [
                        [170, -10],
                        [-170, -10],
                        [-170, 10],
                        [170, 10],
                        [170, -10],
                    ],
                ],
                [
                    [
                        [40, 50],
                        [50, 50],
                        [50, 40],
                        [40, 40],
                        [40, 50],
                    ],
                ],
            ],
        };

        expect(splitMultiPolygon(oneOfTwo)).toEqual({
            type: 'MultiPolygon',
            coordinates: [
                [
                    [
                        [180, 10],
                        [170, 10],
                        [170, -10],
                        [180, -10],
                        [180, 10],
                    ],
                ],
                [
                    [
                        [-180, -10],
                        [-170, -10],
                        [-170, 10],
                        [-180, 10],
                        [-180, -10],
                    ],
                ],
                [
                    [
                        [40, 50],
                        [50, 50],
                        [50, 40],
                        [40, 40],
                        [40, 50],
                    ],
                ],
            ],
        });
    });

    test('Should cut both in MultiPolygon', () => {
        const multiPolygon = {
            type: 'MultiPolygon',
            coordinates: [
                [
                    [
                        [170, -10],
                        [-170, -10],
                        [-170, 10],
                        [170, 10],
                        [170, -10],
                    ],
                ],
                [
                    [
                        [160, -10],
                        [-160, 0],
                        [160, 10],
                        [160, -10],
                    ],
                ],
            ],
        };

        expect(splitMultiPolygon(multiPolygon)).toEqual({
            type: 'MultiPolygon',
            coordinates: [
                [
                    [
                        [180, 10],
                        [170, 10],
                        [170, -10],
                        [180, -10],
                        [180, 10],
                    ],
                ],
                [
                    [
                        [-180, -10],
                        [-170, -10],
                        [-170, 10],
                        [-180, 10],
                        [-180, -10],
                    ],
                ],
                [
                    [
                        [180, 5],
                        [160, 10],
                        [160, -10],
                        [180, -5],
                        [180, 5],
                    ],
                ],
                [
                    [
                        [-180, -5],
                        [-160, 0],
                        [-180, 5],
                        [-180, -5],
                    ],
                ],
            ],
        });
    });

    test('Should cut one with holes in MultiPolygon', () => {
        const hole = [
            [-170, -10],
            [170, -10],
            [170, 10],
            [-170, 10],
            [-170, -10],
        ];

        const cutHole = [
            [
                [-180, 10],
                [-170, 10],
                [-170, -10],
                [-180, -10],
                [-180, 10],
            ],
            [
                [180, -10],
                [170, -10],
                [170, 10],
                [180, 10],
                [180, -10],
            ],
        ];

        const nonCut = [
            [
                [40, 50],
                [50, 50],
                [50, 40],
                [40, 40],
                [40, 50],
            ],
        ];

        const multiPolygon = {
            type: 'MultiPolygon',
            coordinates: [
                [
                    [
                        [160, -20],
                        [-160, -20],
                        [-160, 20],
                        [160, 20],
                        [160, -20],
                    ],
                    hole,
                ],
                nonCut,
            ],
        };

        expect(splitMultiPolygon(multiPolygon)).toEqual({
            type: 'MultiPolygon',
            coordinates: [
                [
                    [
                        [180, 20],
                        [160, 20],
                        [160, -20],
                        [180, -20],
                        [180, 20],
                    ],
                    ...cutHole,
                ],
                [
                    [
                        [-180, -20],
                        [-160, -20],
                        [-160, 20],
                        [-180, 20],
                        [-180, -20],
                    ],
                    ...cutHole,
                ],
                nonCut,
            ],
        });
    });
});
