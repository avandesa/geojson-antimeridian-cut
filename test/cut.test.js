import splitGeoJSON from '../src/cut';

describe('GeoJSON Geometry Objects', () => {
    test('Should not change Point', () => {
        const point = {
            type: 'Point',
            coordinates: [0, 0],
        };
        const result = splitGeoJSON(point);
        expect(result).toEqual(point);
    });

    test('Should not change MultiPoint', () => {
        const multiPointSimple = {
            type: 'MultiPoint',
            coordinates: [
                [0, 0],
                [1, 1],
            ],
        };

        const multiPointCrosses = {
            type: 'MultiPoint',
            coordinates: [
                [170, 0],
                [-170, 0],
            ],
        };

        const resultSimple = splitGeoJSON(multiPointSimple);
        const resultCrosses = splitGeoJSON(multiPointCrosses);

        expect(resultSimple).toEqual(multiPointSimple);
        expect(resultCrosses).toEqual(multiPointCrosses);
    });
});

describe('Miscelaneous GeoJSON Requirements', () => {
    test('Should preserve properties', () => {
        const feature = {
            type: 'Feature',
            properties: { x: 1, foo: 'bar' },
            geometry: null,
        };

        const collection = {
            type: 'FeatureCollection',
            features: [
                {
                    type: 'Feature',
                    properties: { x: 1, foo: 'bar' },
                    geometry: null,
                },
                {
                    type: 'Feature',
                    properties: {},
                    geometry: {
                        type: 'LineString',
                        coordinates: [
                            [170, 0],
                            [-170, 0],
                        ],
                    },
                },
            ],
        };

        const result = splitGeoJSON(feature);
        const resultCollection = splitGeoJSON(collection);

        expect(result).toEqual(feature);
        expect(resultCollection.features[1].properties).toEqual({});
    });

    test('Should preserve foreign members', () => {
        const feature = {
            type: 'Feature',
            properties: null,
            geometry: null,
            foo: 'bar',
        };

        const lineString = {
            type: 'LineString',
            coordinates: [
                [170, 0],
                [-170, 0],
            ],
            x: 1,
        };

        const resultFeature = splitGeoJSON(feature);
        const resultLineString = splitGeoJSON(lineString);

        expect(resultFeature).toEqual(resultFeature);
        expect(resultLineString.x).toEqual(1);
    });
});

describe('Errors & Edge Cases', () => {
    test('Should error without valid type property', () => {
        expect(() => splitGeoJSON({ a: 'foo' })).toThrow();
        expect(() => splitGeoJSON({ type: 1 })).toThrow();
        expect(() => splitGeoJSON({ type: 'foo' })).toThrow();
    });
});
